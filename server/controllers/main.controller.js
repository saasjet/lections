class MainController{
    constructor(app) {
        app.get("/", (req, res) => {
            res.render("index.html", {locals: {title: "Welcome!"}});
        });

        app.post("/", (req, res) => {
            console.log(req.body)
            res.json({message: "It`s post", body: req.body})
        });
    }


}

module.exports = MainController;