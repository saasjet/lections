const express = require("express");
const UserService = require("../services/user.service");


class UserController {

    constructor(app) {

        const userRouter = express.Router();

        const users = new UserService();

        userRouter.get("/all", this.middleware,  async (req, res) => {
            res.json(await users.getAllUsers());
        })

        userRouter.post("/", async (req, res) => {
            res.json(await users.setUser(req.body));
        })

        userRouter.delete("/", async (req, res) => {
            res.json(await users.deleteUserByName(req.body));
        })

        app.use("/user", userRouter);
    }

    middleware(req, res, next) {
        console.log(req.body);
        next();
    }
}

module.exports = UserController;