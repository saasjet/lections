const express = require('express');
const es6Renderer = require("express-es6-template-engine");
const cors = require("cors");
const config = require("./config.json");
const path = require("path");
require("dotenv").config();

console.log("Some log");

const MainController = require("./controllers/main.controller");
const MongoService = require("./services/mongo.service");
const UserController = require("./controllers/user.controller");

const app = express();
app.engine("html", es6Renderer);
app.set("views", path.join(__dirname, "..", "client/public"));
app.use(express.static(path.join(__dirname, "..", "client/public")));
app.use(express.json());
app.use(cors());

(async () => {
    await new MongoService(config.mongo).init();

    new MainController(app);
    new UserController(app);

    app.listen(process.env.PORT, () => {
        console.log(`Server is up on ${process.env.PORT} ${process.env.ENVIRONMENT}!`);
    })
})();

/*
    / 1
    /posts {post: []}
    /post/info text
    /post?id=1 {id: 1, title: "title"}


 */