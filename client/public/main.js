document.getElementById("save").onclick = async function(){
    const users = await fetch("/user/all").then(resp => resp.json());
    await fetch(
        "/user",
        {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: "SomeName",
                surname: "SomeSurname"
            })
        }
    ).then(resp => resp.json())
}